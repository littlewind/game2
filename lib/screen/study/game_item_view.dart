import '../../model/game/game_object.dart';
import '../../model/game/quiz_game_object.dart';
import '../../screen/study/quiz_view.dart';
import '../../screen/study/study_screen.dart';
import 'package:flutter/material.dart';

typedef OnAnswer<T>(AnswerType type, [T params]);

class GameItemView extends StatelessWidget {
  final GameObject gameObject;
  final OnAnswer onAnswer;

  GameItemView({Key key, this.gameObject, this.onAnswer}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (gameObject is QuizGameObject) {
      return QuizView(
        onAnswer: onAnswer,
        gameObject: gameObject,
      );
    } else {
      return Center(
        child: Text("undefined game"),
      );
    }
  }
}
